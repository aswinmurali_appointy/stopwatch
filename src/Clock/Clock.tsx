import React from "react";

import '../App.css';

interface ClockState {
    hours: number,
    minutes: number,
    seconds: number,
}

class CountdownTimer extends React.Component<{}, ClockState> {
    state: Readonly<ClockState>;
    interval: NodeJS.Timer | null;

    constructor(props: {}) {
        super(props);
        this.state = { hours: 0, minutes: 0, seconds: 0 }
        this.interval = null;
    }

    componentWillUnmount() {
        if (this.interval != null)
            clearInterval(this.interval);
    }

    start = () => this.interval = setInterval(() => this.tick(), 1000);

    reset = () => {
        if (this.interval != null)
            clearInterval(this.interval);
        this.setState({ hours: 0, minutes: 0, seconds: 0 })
    }

    tick = () => {
        this.setState({ seconds: this.state.seconds + 1 })

        if (this.state.seconds > 59) {
            this.setState({ seconds: 0, minutes: this.state.minutes + 1 })
        }
        if (this.state.minutes > 59) {
            this.setState({ minutes: 0, hours: this.state.hours + 1 })
        }
    };

    getDisplayUnit = (unit: number) => unit.toString().padStart(2, '0')

    render = () => {
        return <table>
            <tr className='content'>
                <th>
                    <tr>
                        <th className='count'>
                            {this.getDisplayUnit(this.state.hours)} :
                        </th>
                    </tr>
                </th>
                <th>
                    <tr>
                        <th className='count'>
                            {this.getDisplayUnit(this.state.minutes)} :
                        </th>
                    </tr>
                </th>
                <th>
                    <tr>
                        <th className='count'>
                            {this.getDisplayUnit(this.state.seconds)}
                        </th>
                    </tr>
                </th>
            </tr>
            <tr className='content'>
                <th>
                    <input
                    className="theme-button"
                        type={'button'}
                        value={'Start'}
                        onClick={this.start}
                    />
                </th>
                <th className='button-padding'>
                    <input className="theme-button"
                        type={'button'}
                        value={'Reset'}
                        onClick={this.reset}
                    />
                </th>
            </tr>
        </table>;
    }
}

export default CountdownTimer;