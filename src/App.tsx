import './App.css';

import CountdownTimer from './Clock/Clock';

const App = () => (
  <div className='app-container'>
    <div className='app app-window'>
      <p className='content'>React Stopwatch</p>
      <CountdownTimer />
    </div>
  </div>
)

export default App;
